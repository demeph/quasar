#include "ModeMenu.h"
#include <QApplication>

modeMenu::modeMenu(QWidget *parent) : QWidget(parent)
{
    setFixedSize(250,250);
    std::shared_ptr<QPushButton> modeJoueur1(new QPushButton("ModeJoueur",this));
    modeJoueur = modeJoueur1;
    //modeJoueur1.reset();
    modeJoueur->setToolTip("Choissisez la mode humain");
    modeJoueur->move(70,70);
    modeJoueur->setFixedSize(100,30);

    std::shared_ptr<QPushButton> modeIA1(new QPushButton("Mode IA",this));
    modeIA = modeIA1;
    //modeIA1.reset();
    modeIA->setToolTip("Choisissez la mode Ordinateur");
    modeIA->move(70,120);
    modeIA->setFixedSize(100,30);

    std::shared_ptr<QPushButton> menu1(new QPushButton("Menu",parent));
    menu = menu1;
    menu->move(0,0);
    menu->hide();
    menu->setFixedSize(80,20);

    QObject::connect(modeJoueur.get(),SIGNAL(clicked()),this,SLOT(on_modeJoueur_clicked()));
    QObject::connect(modeIA.get(),SIGNAL(clicked()),this,SLOT(on_modeIA_clicked()));
    QObject::connect(menu.get(),SIGNAL(clicked()),this,SLOT(on_menu_clicked()));

    modeJoueur->show();
    modeIA->show();

}



void modeMenu::on_modeJoueur_clicked()
{
    std::shared_ptr<modeHumain> modeJ1(new modeHumain(this));
    modeJ = modeJ1;
    modeJ->show();
    modeJ->cacherArretJeu();
    menu->show();
    modeJoueur->hide();
    modeIA->hide();
}

void modeMenu::on_modeIA_clicked()
{
    menu->show();
    modeIA->hide();
    modeJoueur->hide();
    std::shared_ptr<modeOrdi> ia1(new modeOrdi(this));
    ia = ia1;
    ia->show();
}

void modeMenu::on_menu_clicked()
{
    if (ia){
      ia->hide();
    }

    if (modeJ){
      modeJ->hide();
    }
    modeIA->show();
    modeJoueur->show();
    menu->hide();
}

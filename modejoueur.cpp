#include "modejoueur.h"


modeHumain::modeHumain(QWidget *parent) : QWidget(parent)
{
    aQuinzePoint = false;

    std::shared_ptr<QPushButton> nbUnAHuit1(new QPushButton("1  à  8",this));
    nbUnAHuit_ = nbUnAHuit1;
    nbUnAHuit_->setToolTip("Nombre Aleatoire entre 1 à 8");
    nbUnAHuit_->move(30,130);

    std::shared_ptr<QPushButton> nbQuatreASept1(new QPushButton("4  à  7",this));
    nbQuatreASept_ = nbQuatreASept1;
    nbQuatreASept_->setToolTip("Nombre Aleatoire entre 4 à 7");
    nbQuatreASept_->move(140,130);

    std::shared_ptr<QLCDNumber> lcdEcran1(new QLCDNumber(this));
    lcdEcran_ = lcdEcran1;
    lcdEcran_->setSegmentStyle(QLCDNumber::Flat);
    lcdEcran_->move(70,50);
    lcdEcran_->display(1);
    lcdEcran_->setFixedSize(100,40);    

    nbQuatreASept_->show();
    nbUnAHuit_->show();

    std::shared_ptr<QPushButton> arreterJeu1(new QPushButton("Arreter le partie",this));
    arreterJeu = arreterJeu1;
    arreterJeu->move(70,160);



	QObject::connect(nbUnAHuit_.get(),SIGNAL(clicked()),this,SLOT(on_nbUnAHuit_clicked()));
    QObject::connect(nbQuatreASept_.get(),SIGNAL(clicked()),this,SLOT(on_nbQuatreASept_clicked()));
    QObject::connect(arreterJeu.get(),SIGNAL(clicked()),this,SLOT(on_arreterJeu_clicked()));

    std::shared_ptr<Joueur> j1(new Joueur("Demetre"));
    j = j1;

    balance = j->getBalance();
    std::string msg = "Bonjour " + j1->getNom();

    std::shared_ptr<QLabel> lbl(new QLabel(this));
    lbl_ = lbl;
    lbl->setText(QString::fromStdString(msg));
    lbl->move(70,0);
    lbl->show();

}

void modeHumain::evalueScore(int nb)
{
    if ( nb > 20 )
    {
        perdu();
    }
    else if ( nb == 20 )
    {
        gagne();
    }
    else if ((nb > 14) && (aQuinzePoint==false))
    {
        arreterJeu->show();
        aQuinzePoint =true;
    }
}

void modeHumain::afficherMessage(std::string msg)
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this,"Felicitation",QString::fromStdString(msg),QMessageBox::Yes|QMessageBox::No);

    if ( reply==QMessageBox::Yes)
    {
        recommencerJeu();
    } else {
        lcdEcran_->hide();
        nbUnAHuit_->hide();
        nbQuatreASept_->hide();
        arreterJeu->hide();
    }
}
void modeHumain::gagne()
{
    j->setBalance(40);
    balance = j->getBalance();
    std::string message= "Felicitations, vous avez gagne la partie!!!\n ";
    message += "votre gaine est : 40 €";
    message += "\nVoulez-vous jouer a nouveau?";
    message +="\nOui - pour recommencer\nNo - menu principale";
    afficherMessage(message);
}

void modeHumain::perdu()
{
    std::string message= "Malheuresement vous avez perdu";
    message += "\nVoulez-vous jouer a nouveau?";
    message +="\nOui - pour recommencer\nNo - menu principale";
    afficherMessage(message);
}



void modeHumain::playEngine(std::shared_ptr<iCommand> cmd)
{
    int nb = cmd->execute();
    nb += j->getScore();
    j->setScore(nb);
    lcdEcran_->display(nb);
    evalueScore(nb);
}

void modeHumain::recommencerJeu()
{
    aQuinzePoint = false;
    j->recommencerJeu();
    lcdEcran_->display(1);
    arreterJeu->hide();
}

void modeHumain::on_nbUnAHuit_clicked()
{
    
    std::shared_ptr<iCommand> cmd1(new  NbUnHuit());
    cmd_ = cmd1;
    playEngine(cmd_);
}

void modeHumain::on_nbQuatreASept_clicked()
{
    std::shared_ptr<iCommand> cmd1(new nbFourSeven());
    cmd_ = cmd1;
    playEngine(cmd_);
}

void modeHumain::on_arreterJeu_clicked()
{
    int gainJoueur = j->getScore();
    std::string message= "Felicitations, vous avez gagne la partie!!!\n votre gaine est : ";
    gainJoueur -=15;
    j->setBalance(gain[gainJoueur]);
    balance = j->getBalance();
    message += gainChaine[gainJoueur];
    message += " €";
    message += "\nVoulez-vous jouer a nouveau?";
    message +="\nOui - pour recommencer\nNo - menu principale";
    afficherMessage(message);
}

void modeHumain::cacherArretJeu()
{
    arreterJeu->hide();
}

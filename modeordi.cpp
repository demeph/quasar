#include "modeordi.h"

modeOrdi::modeOrdi(QWidget *parent) : QWidget(parent)
{
    score_ = 1;

    std::shared_ptr<QSpinBox> nbParties1(new QSpinBox(this));
    nbParties = nbParties1;
    nbParties->move(100,130);
    nbParties->setMaximum(5);
    nbParties->setMinimum(1);
    nbParties->show();

    std::shared_ptr<QPushButton> validerNbParties1(new QPushButton("Jouer",this));
    validerNbParties = validerNbParties1;
    validerNbParties->setToolTip("Commencer le jeu d'Ordi");
    validerNbParties->show();
    validerNbParties->move(80,160);

    std::shared_ptr<QLCDNumber> lcdEcran1(new QLCDNumber(this));
    lcdEcran = lcdEcran1;
    lcdEcran->setSegmentStyle(QLCDNumber::Flat);
    lcdEcran->move(70,50);
    lcdEcran->display(1);
    lcdEcran->setFixedSize(100,40);

    QObject::connect(validerNbParties.get(),SIGNAL(clicked()),this,SLOT(play()));

    std::shared_ptr<QLabel> label1(new QLabel(this));
    label_ = label1;
    label_ -> setText("");
    label_->move(0,30);
    label_ -> setFixedWidth(250);
    label_->show();

}

modeOrdi::Etat modeOrdi::evaluerScore(int nbGenerer)
{
    score_ += nbGenerer;
    Etat etat;
    if ( score_ > 20)
    {
        etat = perdu;
    }
    else if (score_ == 20)
    {
        etat = gagne;
    }
    else
    {
        etat = jouer;
    }
    lcdEcran->display(score_);
    return etat;
}

void modeOrdi::play()
{
    int  score;
    score_=1;
    score = 20-score_;
    Etat r = jouer;
    //int nbTours= nbParties->value();
    int nbAleat,temp;
    int stop = 1;
    while (stop != 0)
    {
        switch (r)
        {
        case gagne :
            if (score_ == 20)
            {
                label_->setText("Ordinateur a gagne 40 €");
            }
            else
            {
                label_->setText("Ordinateur a gagne");
            }
            r = arret;
            break;
        case perdu:
            label_->setText("Ordinateur a perdu la partie");
            r = arret;
            break;
        case jouer:
            if (score < 5)
            {
                temp = outilsMath->nbAleat(10);
                if (temp >= 2)
                {
                   r = gagne;
                }
                else
                {
                    int temp1 = outilsMath->nbAleat(5);
                    if (temp1 > 2 )
                    {
                        nbAleat = outilsMath ->nbAleatUnHuit();
                    }
                    else
                    {
                        nbAleat = outilsMath -> nbAleat47();

                    }
                }
            }
            else if (score < 8)
            {
                nbAleat = outilsMath -> nbAleatUnHuit();
            }
            else if (score >= 8)
            {
                temp = outilsMath->nbAleat(2);
                if (temp == 0)
                {
                    nbAleat = outilsMath-> nbAleatUnHuit();
                }
                else
                {
                    nbAleat = outilsMath -> nbAleat47();
                }
            }
            r = evaluerScore(nbAleat);
            break;
         case arret:
            stop = 0;
            break;
        }
    }
}

void modeOrdi::on_validerNbPArties_clicked()
{

}

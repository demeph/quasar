#ifndef MODEORDI_H
#define MODEORDI_H

#include <memory>
#include <QObject>
#include <QWidget>
#include <QSpinBox>
#include <QPushButton>
#include <QLabel>
#include <QLCDNumber>
#include "calculamath.h"

class modeOrdi :public QWidget
{
    Q_OBJECT
public:
    explicit modeOrdi(QWidget *parent = 0);

signals:

public slots:
    void on_validerNbPArties_clicked();
    void play();

private:
    std::shared_ptr<QSpinBox> nbParties;
    std::shared_ptr<QPushButton> validerNbParties;
    std::shared_ptr<QLCDNumber> lcdEcran;
    std::shared_ptr<QLabel> label_;
    std::shared_ptr<calculMath> outilsMath;
    int score_;
    enum Etat {gagne, perdu, jouer, arret};

    Etat evaluerScore(int nbGenerer);

};

#endif // MODEORDI_H

#include "jeu.h"
#include "ModeMenu.h"
#include "modeordi.h"
#include "modejoueur.h"

Jeu::Jeu()
{
    setFixedSize(250,250);
    std::shared_ptr<QStackedWidget> leswidgets(new QStackedWidget(this));
    leswidgets_ = leswidgets;
    leswidgets_->addWidget(new modeMenu(this));
    leswidgets_->addWidget(new modeHumain(this));
    leswidgets_->addWidget(new modeOrdi(this));
    std::shared_ptr<QVBoxLayout> layout(new QVBoxLayout());
    layout_ = layout;
    layout_->addWidget(leswidgets_.get());
    setLayout(layout_.get());
}

Jeu::~Jeu()
{

}

SOURCES += \
    main.cpp \
    ModeMenu.cpp \
    modejoueur.cpp \
    modeordi.cpp \
    nbunhuit.cpp \
    calculamath.cpp \
    nbfourseven.cpp \
    joueur.cpp \
    jeu.cpp
    QT += widgets

HEADERS += \
    ModeMenu.h \
    modejoueur.h \
    modeordi.h \
    iCommand.h \
    nbunhuit.h \
    calculamath.h \
    nbfourseven.h \
    joueur.h \
    jeu.h

CONFIG += c++11
